function clearMarkers (markers) {
  // Clear Marker Function
  
  for (let i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
  markers = [];
  return markers;
}
function addMarker (markers, position, username, timestamp) {
  // Add Marker Function
  // Calculate time hour from device timestamps
  let now = new Date();
  let res = Math.abs(now - timestamp) / 1000;
  let hours = Math.floor(res / 3600) % 24;

  // Check Hourse
  if (hours < 1) {
    // If hours < 1 = active animation in marker
    markers.push(
      new window.google.maps.Marker({
        position: position,
        title: username,
        animation: window.google.maps.Animation.BOUNCE
      })
    );
  } else {
    // else disable animation in marker
    markers.push(
      new window.google.maps.Marker({
        position: position,
        title: username
      })
    );
  }
  return markers;
}

export { clearMarkers, addMarker };
