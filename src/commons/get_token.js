
/* eslint-disable */
function getToken() {
  let token = window.localStorage.getItem('token');
  return token;
}

function getPhone() {
  let phone = window.localStorage.getItem('phone');
  return phone;
}

export { getToken, getPhone };
