import Vue from 'vue';

import Vuex from 'vuex';

import emergency_store from './emergency_store';
import posko_ungsi_store from './posko_ungsi_store';
import posko_bantuan_store from './posko_bantuan_store';
import user_store from './user_store';
import rumah_rusak_store from './rumah_rusak_store';
import fasilitas_umum_store from './fasilitas_umum_store';
import faskes_store from './faskes_store';
import fasdik_store from './fasdik_store';


Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    emergencyStore: emergency_store,
    poskoUngsiStore: posko_ungsi_store,
    userStore: user_store,
    poskoBantuanStore: posko_bantuan_store,
    rumahRusakStore: rumah_rusak_store,
    fasilitasUmumStore: fasilitas_umum_store,
    faskesStore: faskes_store,
    fasdikStore: fasdik_store,
  }
});
