import { firestore } from '../commons/firebase_common';

export default {
  namespaced: true,
  state: {
    rumahs: []
  },
  getters: {
    getRumahs (state) {
      return state.rumahs;
    }
  },
  mutations: {
    setRumahs (state, rumah) {
      state.rumahs.push(rumah);
    },
    addRumah (state, rumah) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('rumahrusak')
        .collection('list')
        .add(rumah).then(res => {
          console.log(res);
        }).catch(error => {
          console.log(error);
        });
    },
    clearRumahs (state) {
      state.rumahs = [];
    },
    fetchRumahs (state) {
      firestore
        .collection('data')
        .doc('KOGASGABPADGEMPANTB')
        .collection('data')
        .doc('rumahrusak')
        .collection('list')
        .onSnapshot(doc => {
          state.rumahs = [];
          for (let i = 0; i < doc.docs.length; i++) {
            state.rumahs.push(doc.docs[i].data());
          }
        });
    }
  },
  actions: {
    fetchRumahs: ({ commit }) => {
      commit('fetchRumahs');
    },
    addRumah: ({ commit }, rumah) => {
      commit('addRumah', rumah);
    },
    setRumahs: ({ commit }, rumah) => {
      commit('setRumahs', rumah);
    },
    clearRumahs: ({ commit }) => {
      commit('clearRumahs');
    }
  }
};
